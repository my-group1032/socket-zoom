import http from "http";
import SocketIO from "socket.io";
import express from "express"; // express 어플리케이션을 구성
import { type } from "os";

const app = express();

app.set("view engine", "pug"); // 화면의 파일 타입지정
app.set("views", __dirname + "/public/views"); // view화면의 경로 지정
app.use("/public", express.static(__dirname + "/public"));
app.get("/", (req, res) => res.render("home")); // 메인페이지로 연결해줌
app.get("/*", (req, res) => res.redirect("/")); // 어디로 링크를 이동하든 메인으로 돌아오게 연결

const handleListen = () => console.log(`Listening on http://localhost:3000`);

const httpServer = http.createServer(app);  // http 생성
const wsServer = SocketIO(httpServer);

wsServer.on("connection", (socket) => {
    socket.on("enter_room", (msg, done) => {
        console.log(msg);
        setTimeout(() => {
            done();
        }, 10000);
    });
});

// 동일한 포트에 http, ws 둘다 생성

/*
const wss = new WebSocket.Server({ server });  // ws 생성
const sockets = [];  // 사용하는 웹사이트모음
wss.on("connection", (socket) => {
    sockets.push(socket);  // 사용하는 웹사이트들을 socket에 넣는다.
    socket["nickname"] = "Anon";
    console.log("Connected to Browser ✅");
    socket.on("close", () => console.log("Disconnected from the Browser ❌"));
    socket.on("message", (msg) => {
        const message = JSON.parse(msg);
        switch (message.type) {
            case "new_message":
                sockets.forEach(aSockts => aSockts.send(`${socket.nickname}: ${message.payload}`));  // 사용하는 홈페이지마다 message 전송
                break;
            case "nickname":
                socket["nickname"] = message.payload;
                break;
        }
    });
});
*/

httpServer.listen(3000, handleListen);
